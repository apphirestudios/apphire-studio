module.exports =
  cleanup: (callback) ->
    process.on 'exit', (exitCode)->
      callback()
      process.exit(exitCode)
     
    # catch ctrl+c event and exit normally
    process.on 'SIGINT', ->
      console.log 'Ctrl-C...'
      process.exit 2

    process.on 'SIGTERM', ->
      console.log 'Process terminated...'
      process.exit 2
      
    #catch uncaught exceptions, trace, then exit normally
    process.on 'uncaughtException', (e) ->
      console.error 'Uncaught Exception...'
      console.error e.stack or e
      setTimeout ( -> process.exit 2), 200