Apphire = require 'apphire'
require '../watched'

apphire = new Apphire(process.env)


apphire.router.post "/test/echo", (next)->     
  @body = @request.body
  yield next
  
apphire.router.get "/test/get", (next)->     
  @body = {ok: 'OK'}
  yield next

apphire.start().catch (e)->
  console.error e.stack or e
  setTimeout (-> process.exit(1)), 500  