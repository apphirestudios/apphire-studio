Emitter = require 'apphire/lib/emitter'
path = require 'path'
async = require 'co'
dirTree = require './directory-tree'
fs = require 'fs'
chokidar = require 'chokidar'

debug = (set)->
  if module.exports.debug then console.log set  

resolveRealPath = (filePath)->
  return path.join cwd, filePath

normalizeBranchPaths = (branch, cwd)->
  for leaf in branch
    leaf.path = '/' + path.relative(cwd, leaf.path)
    if leaf.children?
      normalizeBranchPaths(leaf.children, cwd)

fetchTree = ()-> async =>
  root = dirTree(cwd, [/\/\.git\//]) or {}
  root.name = '/'
  root.path = '/'
  normalizeBranchPaths(root.children, cwd)
  return root

class VFS extends Emitter
  watch: (files)-> async =>
    @_watcher?.close()
    @_watcher = chokidar.watch files,
      ignoreInitial: true
      ignored: /node_modules/
    @_watcher.on 'all', (event, p)=>  
      @emit 'file:change', 
        event: event
        path: p
      @app.socket.broadcast 'file:change',
        event: event
        path: '/' + path.relative cwd, p
  unwatch: ()-> async =>
    @_watcher?.close()
  constructor: (@app)->
    super()
    
    @app.socket.response 'fetchTree', (req)->
      return yield fetchTree()
    
    @app.socket.response 'readFile', (req) -> 
      return fs.readFileSync(resolveRealPath(req.filePath), req.encoding)  
    
    @app.socket.response 'stat', (req) ->
      stat = fs.statSync(resolveRealPath(req.filePath))
      if stat.isFile() then return 0
      else if stat.isDirectory() then return 1
      else return -1

    @app.socket.response 'internalStat', (req) ->
      try
        stat = fs.statSync(resolveRealPath(req.filePath))
      catch e
        return -1
      if stat.isFile() then return 0
      else if stat.isDirectory() then return 1
      else return -1

    @app.socket.response 'writeFile', (req) -> 
      return fs.writeFileSync(resolveRealPath(req.filePath), req.content)
         
module.exports = VFS

