Apphire = require 'apphire/lib/backend'
VFS = require './vfs/backend'
fs = require 'fs'
path = require 'path'
fork = require('child_process').fork
express = require 'express'
httpProxy = require 'http-proxy'
webpack = require 'webpack'
webpackDevMiddleware = require 'webpack-dev-middleware'
webpackHotMiddleware = require 'webpack-hot-middleware'
async = require 'co'
net = require('net')

portIsFree = (port)-> async ->
  return yield new Promise (resolve, reject)->
    server = net.createServer()
    server.once 'error', (err) ->
      if err.code == 'EADDRINUSE'
        resolve false
      else
        reject err
    server.once 'listening', ->
      server.close()
      resolve true
    server.listen(port)

process.env.NODE_ENV ?= "development"

checkIfInitialized = ->
  if not @initialized then throw new Error "Studio is not initialized. You must call initialize method first."

class Studio extends Apphire
  constructor: (@webpackConfig, @appBackendEntry, @staticPath)->
    @vfs = new VFS(@)
    require('./cleanup').cleanup =>
      @_stopAppBackend()
    super()

  initialize: -> async =>
    freePorts = []
    port = 3000
    while not yield portIsFree(port)
      port += 2
    @WEBPACK_PORT = port
    @PORT = port + 1
    process.env.BACKEND_URL = "http://#{process.env.IP or 'localhost'}:#{@WEBPACK_PORT}"
    process.env.SOCKET_URL = "ws://#{process.env.IP or 'localhost'}:#{@PORT}"
    @initialized = true

  build: -> async =>
    if process.env.NODE_ENV is 'development' then checkIfInitialized.call(@)
    compiler = webpack(@webpackConfig)
    yield new Promise (resolve, reject)->
      compiler.run (err, stats)->
        if err then reject err else resolve stats
    log "Successfully built!"

  develop: ()-> async =>
    checkIfInitialized.call(@)
    if @appBackendEntry
      yield @_startAppBackend()
      yield @_startWatchingBackendSources()
    yield @_startWebpackDevServer()
    log "Watching sources... open #{process.env.BACKEND_URL} to start developing"

  stop: -> async =>
    yield @_stopWatchingBackendSources()
    @_stopAppBackend()

  _startWebpackDevServer: -> async =>
    @webpackConfig.devtool = 'eval'
    @webpackConfig.entry ?= []
    @webpackConfig.entry.push "webpack-hot-middleware/client?path=#{process.env.BACKEND_URL}/__webpack_hmr"
    @webpackConfig.plugins ?=[]
    @webpackConfig.plugins.push new webpack.HotModuleReplacementPlugin()
    @webpackConfig.output ?= {}
    @webpackConfig.output.hotUpdateChunkFilename = 'hot/hot-update.js'
    @webpackConfig.output.hotUpdateMainFilename = 'hot/hot-update.json'
    @webpackConfig.output.publicPath = "#{process.env.BACKEND_URL}/js/"
    @webpackDevServer = wepbackDevServer = express()
    apiProxy = httpProxy.createProxyServer()
    compiler = webpack(@webpackConfig)
    wepbackDevServer.use webpackDevMiddleware(compiler, publicPath: @webpackConfig.output.publicPath)
    wepbackDevServer.use webpackHotMiddleware(compiler)
    self = @
    if @staticPath then wepbackDevServer.use express.static(@staticPath)
    wepbackDevServer.use '/*', (req, res) ->
      req.url = req.baseUrl
      # Janky hack...
      apiProxy.web req, res, target: {port: self.PORT, host: 'localhost'}, (err)->
        if err
          res.writeHead 500, 'Content-Type': 'text/plain'
          res.end(err.toString())
      return
    yield new Promise (resolve, reject)=>
      wepbackDevServer.listen @WEBPACK_PORT, (err)->
        if err then reject(err) else resolve()

  _startAppBackend: ()-> async =>
    @appEnv = {}
    for k, v of process.env #We copy all env from current process first
      @appEnv[k] = v

    @appEnv.PORT = @PORT
    yield new Promise (resolve, reject)=>
      @appBackend = fork @appBackendEntry, [],
        env: @appEnv
        silent: true
      @appBackend.stdout.on 'data', (message)=>
        @socket.broadcast 'backend:stdout', message.toString()
        @notify message.toString()
        if message.indexOf('HTTP server listening on') is 0 then resolve()
      @appBackend.stderr.on 'data', (message)=>
        @socket.broadcast 'backend:stderr', message.toString()
        @error message.toString()
      @socket.broadcast 'backend:start'

  _startWatchingBackendSources: -> async =>
    watchedFiles = yield @request.get("http://localhost:#{@PORT}/__required__")
    @vfs.on 'file:change', (fileEvent)=> async =>
      if watchedFiles.indexOf(fileEvent.path) isnt -1
        yield @_restartAppBackend()
    yield @vfs.watch(watchedFiles)

  _stopWatchingBackendSources: -> async =>
    yield @vfs.unwatch()

  _restartAppBackend: ()-> async =>
    @._stopAppBackend()
    yield @._startAppBackend()
    yield @_stopWatchingBackendSources()
    yield @_startWatchingBackendSources()
    @socket.broadcast 'backend:restart'

  _stopAppBackend: ()->
    @.appBackend?.kill()

module.exports = Studio