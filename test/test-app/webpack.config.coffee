webpack = require 'webpack'
path = require 'path'

module.exports =
  devtool: 'eval'
  entry: [
    require.resolve 'webpack-hot-middleware/client'
    path.join __dirname, './src/app'
  ]
  output:
    path: path.join(__dirname, 'dist')
    filename: 'bundle.js'
    publicPath: '/static/'
  plugins: [ new (webpack.HotModuleReplacementPlugin) ]
  resolve:
    extensions: [
      ''
      '.js'
      '.jsx'
      '.coffee'
      '.csx'
      '.json'
    ]
    fallback: path.join(__dirname, "../node_modules")
  module:
    exprContextRegExp: /$^/,
    exprContextCritical: false,
    loaders: [
      {
        test: /\.coffee$/
        loaders: [ 'coffee' ]
      }
      {
        test: /\.csx$/
        loaders: [ 'apphire-csx-loader' ]
      }
      {
        test: /\.json$/
        loaders: [ 'json' ]
      }
    ]
  node:
    fs: "empty"
    child_process: "empty"