path = require 'path'
Studio = require './backend'
global.cwd = process.cwd()

args = process.argv.slice(2)
if not command = args[0] then throw new Error 'Command not specified'
backendEntry = args[1]
webpackConfig = require(path.join(cwd, './webpack.config'))

studio = new Studio(webpackConfig, backendEntry, null)
do -> async ->
  try
    yield studio.initialize()
    switch command
      when 'build'
        yield studio.build()
      when 'develop'
        if not backendEntry then throw new Error 'Backend entry point is not specified'
        yield studio.develop()
  catch e
    console.error e.stack or e
    setTimeout (-> process.exit(1)), 500

