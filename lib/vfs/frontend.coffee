Emitter = require 'apphire/lib/emitter'

class VFS extends Emitter
  _cache: {}
  _statCache: {}
  _internalStatCache: {}
  client: undefined
  constructor: (@app)->
    super()
    @app.socket.on 'file:change', (e)=>
      @emit 'file:change', e
  
  writeFile: (filePath, content)-> async =>
    return yield @app.socket.request("writeFile", { filePath: filePath, content: content })

  readFile: (filePath, encoding)-> async =>
    encoding ?= 'utf-8'
    if @_cache[filePath] then return @_cache[filePath]
    return @_cache[filePath] = yield @app.socket.request("readFile", { filePath: filePath, encoding: encoding })
    
  stat: (filePath)-> async => 
    if @_statCache[filePath] then return @_statCache[filePath]
    response = yield @app.socket.request("stat", { filePath: filePath })
    return @_statCache[filePath] =
      isFile: -> return response is 0
      isDirectory: -> return response is 1

  internalStat: (filePath)-> async =>
    if @_internalStatCache[filePath] then return @_internalStatCache[filePath]
    return @_internalStatCache[filePath] = yield @app.socket.request("internalStat", { filePath: filePath })

module.exports = VFS