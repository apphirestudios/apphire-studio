directoryTree = (path, filters) ->
  name = PATH.basename(path)
  item = 
    path: path
    name: name
  stats = undefined
  try
    stats = FS.statSync(path)
  catch e
    return null
  if stats.isFile()
    ext = PATH.extname(path).toLowerCase()
    if filters and filters.length
      for filter in filters
        if filter.test path then return null
    item.size = stats.size
    # File size in bytes
    item.extension = ext
  else if stats.isDirectory()
    try
      item.children = FS.readdirSync(path).map((child) ->
        directoryTree PATH.join(path, child), filters
      ).filter((e) ->
        ! !e
      )
      if !item.children.length
        return null
      item.size = item.children.reduce(((prev, cur) ->
        prev + cur.size
      ), 0)
    catch ex
      if ex.code == 'EACCES'
        return null
  else
    return null
    # Or set item.size = 0 for devices, FIFO and sockets ?
  item

'use strict'
FS = require('fs')
PATH = require('path')
module.exports = directoryTree
