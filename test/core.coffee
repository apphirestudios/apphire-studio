fs = require 'fs'
fork = require('child_process').fork
process.execArgv.pop()
#remove ['--debug'] from arguments list for avoiding EADDRINUSE
#when trying to fork processes
SocketClientNode = require './socket-client-node'
SocketClient = require 'apphire/lib/socket-client'
SocketClient._WSClient = SocketClientNode

studioEnv =
  NODE_ENV: 'development'
  PORT: '3000'
  WEBPACK_PORT: '8080'

studioBackendAdress = "http://localhost:#{studioEnv.PORT}"
studioBackendWS = "ws://localhost:#{studioEnv.PORT}"
webpackDevMiddlewareAdress = "http://localhost:#{studioEnv.WEBPACK_PORT}"

debug = false

describe "apphire-studio", ->
  studioFrontend = undefined
  forkedStudio = undefined
  spawnStudio = ->
    forkedStudio = fork '../../bin/apphire-studio', ['./bin/backend'],
      cwd: './test-app'
      env: studioEnv
      silent: true
    
    yield new Promise (resolve, reject)->
      forkedStudio.stdout.on 'data', (message)->
        if debug
          log '' + message
        if message.indexOf('Apphire studio is ready') is 0 then resolve()
      forkedStudio.stderr.on 'data', (e)->
        console.error e.stack
        reject new Error e

    global.document =
      readyState: 'not complete'
      addEventListener: (name, fn)->
        if name is 'DOMContentLoaded' then setTimeout fn, 100

    studioFrontend = new (require '../lib/frontend')

    studioFrontend._getSocketAdress = ->
      return studioBackendWS

    app = yield studioFrontend.launch()
    if debug
      studioFrontend.socket.on '**', (data)->
        log "EVENT: #{@event} DATA: #{JSON.stringify(data)}"
    app.should.equal studioFrontend

  killStudio = ->
    forkedStudio.kill()
    fs.writeFileSync('./test-app/watched.js', 'module.exports = 10;')

  before spawnStudio
  after killStudio

  describe "stutio", ->
    it "Gets studio index html", ->
      studioIndexHtml = fs.readFileSync '../lib/studio.html', 'utf-8'
      yield request(studioBackendAdress).get("/studio").expect(200, studioIndexHtml).end()

  describe "vfs", ->
    it "readFile: ok", ->
      file = yield studioFrontend._vfs.readFile('/watched.js')
      file.should.equal fs.readFileSync './test-app/watched.js', 'utf-8'

    it "readFile: not-found", ->
      yield throws studioFrontend._vfs.readFile('/not-exist'), /ENOENT: no such file or directory/

    it "stat: file", ->
      stat = yield studioFrontend._vfs.stat('/watched.js')
      stat.isFile().should.equal true
      stat.isDirectory().should.equal false

    it "stat: dir", ->
      stat = yield studioFrontend._vfs.stat('/src')
      stat.isFile().should.equal false
      stat.isDirectory().should.equal true

    it "stat: not-found", ->
      yield throws studioFrontend._vfs.stat('/not-exist'), /ENOENT: no such file or directory/

    it "internalStat: file", ->
      stat = yield studioFrontend._vfs.internalStat('/watched.js')
      stat.should.equal 0

    it "internalStat: dir", ->
      stat = yield studioFrontend._vfs.internalStat('/src')
      stat.should.equal 1

    it "internalStat: not-found", ->
      stat = yield studioFrontend._vfs.internalStat('/not-exist')
      stat.should.equal -1

    it "writeFile: ok", ->
      content = 'module.exports = 20;'
      file = yield studioFrontend._vfs.writeFile('/watched.js', content)
      (fs.readFileSync('./test-app/watched.js', 'utf-8')).should.equal content


    it "writeFile: not a file", ->
      content = 'module.exports = 20;'
      yield throws studioFrontend._vfs.writeFile('/src', content), /EISDIR: illegal operation on a directory/

  describe "restart", ->
    it "Restarts backend on change", ->
      yield new Promise (resolve)->
        setTimeout (-> fs.writeFileSync('./test-app/watched.js', 'module.exports = 10;')), 100
        studioFrontend._vfs.once 'file:change', (e)->
          if e.path is '/watched.js' then resolve() else reject(new Error 'Path mismath, got ' + e.path)
      yield new Promise (resolve)->
        studioFrontend.socket.once 'backend:restart', (e)->
          resolve()

    it "Handles backend errors properly", ->
      yield new Promise (resolve)->
        setTimeout (-> fs.writeFileSync('./test-app/watched.js', 'throw new Error("ERR");')), 200
        setTimeout (-> fs.writeFileSync('./test-app/watched.js', 'module.exports = 10;')), 400
        studioFrontend.socket.once 'backend:restart', (e)->
          resolve()

  describe "get", ->
    it "get", ->
      resp = yield request(webpackDevMiddlewareAdress).get("/test/get").expect(200, {ok: 'OK'}).end()

    it "echo", ->
      echo = {ok: 'ECHO'}
      yield request(webpackDevMiddlewareAdress).post("/test/echo").send(echo).expect(200, echo).end()

  describe "webpack", ->
    it "get bundle.js", ->
      yield request(webpackDevMiddlewareAdress).get("/static/bundle.js").expect(200).end()




